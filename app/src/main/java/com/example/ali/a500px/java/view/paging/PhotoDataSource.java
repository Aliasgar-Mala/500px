package com.example.ali.a500px.java.view.paging;

import android.util.Log;

import com.example.ali.a500px.java.model.Photo;
import com.example.ali.a500px.java.model.PhotoResponse;
import com.example.ali.a500px.java.networking.RetrofitClient;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoDataSource extends PageKeyedDataSource<Integer, Photo> {

    private static final int FIRST_PAGE = 1;

    @Override
    public void loadInitial(@NonNull final LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Photo> callback) {

        Call<PhotoResponse> call = RetrofitClient.getInstance()
                .getService()
                .getPhotos(FIRST_PAGE);

        call.enqueue(new Callback<PhotoResponse>() {
            @Override
            public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {
                callback.onResult(Arrays.asList(response.body().getPhotos()), null, FIRST_PAGE +1 );
            }

            @Override
            public void onFailure(Call<PhotoResponse> call, Throwable t) {
                Log.d("error", "why");
            }
        });

    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Photo> callback) {

        Call<PhotoResponse> call = RetrofitClient.getInstance()
                .getService()
                .getPhotos(params.key);

        call.enqueue(new Callback<PhotoResponse>() {
            @Override
            public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {

                Integer key  = params.key > 1 ? params.key - 1 : null;

                callback.onResult(Arrays.asList(response.body().getPhotos()), key );
            }

            @Override
            public void onFailure(Call<PhotoResponse> call, Throwable t) {
                Log.d("error", "why");
            }
        });

    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Photo> callback) {

        Call<PhotoResponse> call = RetrofitClient.getInstance()
                .getService()
                .getPhotos(params.key);

        call.enqueue(new Callback<PhotoResponse>() {
            @Override
            public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {

                Integer key  = response.body().getCurrentPage() <= response.body().getTotalPages() ? params.key + 1 : null;

                callback.onResult(Arrays.asList(response.body().getPhotos()), key );
            }

            @Override
            public void onFailure(Call<PhotoResponse> call, Throwable t) {
                Log.d("error", "why");
            }
        });

    }
}
