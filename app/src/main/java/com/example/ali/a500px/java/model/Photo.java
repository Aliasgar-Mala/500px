package com.example.ali.a500px.java.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Photo implements Parcelable {

    public String getId() {
        return id;
    }

    private String id;

    public String[] getImageUrl() {
        return imageUrl;
    }
    @SerializedName("image_url")
    private String[] imageUrl;

    public String getName() {
        return name;
    }

    private String name;

    public User getUser() {
        return user;
    }

    private User user;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeStringArray(this.imageUrl);
        dest.writeString(this.name);
        dest.writeParcelable(this.user, flags);
    }

    public Photo() {
    }

    protected Photo(Parcel in) {
        this.id = in.readString();
        this.imageUrl = in.createStringArray();
        this.name = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}

