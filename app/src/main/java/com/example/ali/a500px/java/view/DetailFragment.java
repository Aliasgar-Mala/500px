package com.example.ali.a500px.java.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ali.a500px.R;
import com.example.ali.a500px.java.model.Photo;
import com.example.ali.a500px.java.viewmodel.PhotoViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class DetailFragment extends Fragment {

    private ImageView myImage;
    private TextView userName, imageName;
    private PhotoViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        myImage = view.findViewById(R.id.image_view);
        userName = view.findViewById(R.id.user_name);
        imageName = view.findViewById(R.id.image_name);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel =  ViewModelProviders.of(getActivity()).get(PhotoViewModel.class);
        viewModel.getPhoto().observe(getViewLifecycleOwner(), new Observer<Photo>() {
            @Override
            public void onChanged(Photo photo) {
                String imageURL = getResources().getBoolean(R.bool.isTablet) ? photo.getImageUrl()[2] : photo.getImageUrl()[1];
                Glide.with(DetailFragment.this).load(imageURL)
                        .into(myImage);
                imageName.setText(photo.getName() != null ? photo.getName(): "Untitled");
                userName.setText(getResources().getString(R.string.user_name,
                        photo.getUser().getUsername() ));

            }
        });

    }
}
