package com.example.ali.a500px.java.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PhotoResponse implements Parcelable {

    @SerializedName("total_pages")
    private int totalPages;

    private int total_items;

    public Photo[] getPhotos() {
        return photos;
    }

    private Photo[] photos;

    @SerializedName("current_page")
    private int currentPage;

    public int getTotalPages() {
        return totalPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.totalPages);
        dest.writeInt(this.total_items);
        dest.writeTypedArray(this.photos, flags);
        dest.writeInt(this.currentPage);
    }

    public PhotoResponse() {
    }

    protected PhotoResponse(Parcel in) {
        this.totalPages = in.readInt();
        this.total_items = in.readInt();
        this.photos = in.createTypedArray(Photo.CREATOR);
        this.currentPage = in.readInt();
    }

    public static final Creator<PhotoResponse> CREATOR = new Creator<PhotoResponse>() {
        @Override
        public PhotoResponse createFromParcel(Parcel source) {
            return new PhotoResponse(source);
        }

        @Override
        public PhotoResponse[] newArray(int size) {
            return new PhotoResponse[size];
        }
    };
}
