package com.example.ali.a500px.java.networking;

import com.example.ali.a500px.BuildConfig;
import com.example.ali.a500px.java.model.PhotoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Service {

    @GET("photos?feature=popular&image_size=2,3,4&consumer_key="+BuildConfig.ApiKey)
    Call<PhotoResponse> getPhotos(
            @Query("page") int page
    );

}
