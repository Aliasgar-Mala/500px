package com.example.ali.a500px.java.model;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    private String fullname;

    public String getUsername() {
        return fullname;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fullname);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.fullname = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
