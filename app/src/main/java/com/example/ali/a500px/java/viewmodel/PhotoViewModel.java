package com.example.ali.a500px.java.viewmodel;


import com.example.ali.a500px.java.model.Photo;
import com.example.ali.a500px.java.view.paging.PhotoDataSourceFactory;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

public class PhotoViewModel extends ViewModel {

    public LiveData<PagedList<Photo>> photoPagedList;
    private MutableLiveData<Photo> photoMutableLiveData = new MutableLiveData<>();

    public PhotoViewModel() {
        PhotoDataSourceFactory photoDataSourceFactory = new PhotoDataSourceFactory();

        PagedList.Config config = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setPageSize(2)
                .build();

        photoPagedList = new LivePagedListBuilder(photoDataSourceFactory, config).build();
    }

    public void setPhoto(Photo photo) {
        photoMutableLiveData.setValue(photo);
    }

    public LiveData<Photo> getPhoto() {
        return photoMutableLiveData;
    }
}
